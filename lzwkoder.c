#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vectorchar.h"
#include "hash.h"
#include "files.h"


/*
Funkcija koja cita binarni file, stvara rijecnik,
zapisuje u dinamicki char array te ga vraca.
*/
vectorChar *doBrainz(vectorChar* vectorIn){
	vectorChar *vectorOut = (vectorChar*)malloc(sizeof(vectorChar));
	initVector(vectorOut);
	int start = 0;
	ushort dictValue, lastDictValue=-1;
	vectorChar *tmp = (vectorChar*)malloc(sizeof(vectorChar));
	initVector(tmp);

	vectorChar *w = (vectorChar*)malloc(sizeof(vectorChar));
	initVector(w);

	int found;
	char K;
	while(start < vectorIn->count){
		
		reset(tmp);
		vectorCopy(tmp, w); // tmp = w
		K = vectorIn->vector[start];
		addElements(tmp, &K, 1); // tmp = w + bytearr[current]
		dictValue = getDictionary(tmp->vector, tmp->count, &found);

		if(found==1){ // ta rijec postoji u rijecniku
			vectorCopy(w, tmp);
		}else{
			lastDictValue = getDictionary(w->vector, w->count, &found);
			char rijecRazlomljena[2];
			rijecRazlomljena[1] = lastDictValue & 0xFF;
			rijecRazlomljena[0] = (lastDictValue>>8) & 0xFF; // ili (lastDictValue & 0xFF00)>>8
			// prvo se zapisuje onaj najveci byte
			// 0x1234 -> zapisuje u file ovo: 0x12 0x34
			addElements(vectorOut, rijecRazlomljena, 2);
			addDictionary(tmp->vector, tmp->count);
			reset(w);
			addElements(w, &vectorIn->vector[start], 1);

		}
		start++;
	}
	return vectorOut;
}



int main(int numOfArgs, char**args){
	if(0 && numOfArgs!=3){
		puts("Usage: imeprograma ulaznifile izlaznifile\n");
		exit(-1);
	}
	
	FILE *ulazna = readFile(args[1]); //args[1]);
	FILE *izlazna = openWriteBinaryFile(args[2]);
	setHashTable();
	vectorChar *vecIn = getVectorArrayFromFile(ulazna);
	vectorChar *vecOut = doBrainz(vecIn);
	writeToBinaryFile(izlazna, vecOut);
	//printf("Velicina prije %d, velicina poslije %d\n", vecIn->count, vecOut->count);
	//printf("Smanjeno za %f\n", (1.0 - (float)vecOut->count/vecIn->count)*100);

	return 0;
}
