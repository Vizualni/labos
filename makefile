huffkoder:
	/usr/bin/gcc huffkoder.c -o huffkoder

lzwkoder:
	/usr/bin/gcc lzwkoder.c -o lzwkoder

lzwdekoder:
	/usr/bin/gcc lzwdekoder.c -o lzwdekoder

lzwtest: lzwkoder lzwdekoder
	./lzwkoder izvorista/i2.txt test.out && ./lzwdekoder test.out test.out.stari && diff -q izvorista/i2.txt test.out.stari

clear:
	rm huffkoder; rm lzwkoder

runhuffkodertest: huffkoder
	./huffkoder