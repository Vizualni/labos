#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vectorchar.h"

#ifndef FILES_INCLUDED
#define FILES_INCLUDED

FILE * readFile(char *filename){
	FILE *f = fopen(filename, "rb");
	if(!f){
		puts("Nemoze procitat file\n");
		exit(-1);
	}
	return f;
}


FILE *openWriteBinaryFile(char *filename){
	FILE *f = fopen(filename, "wb");
	if(!f){
		puts("Nemoze procitat file\n");
		exit(-1);
	}
	return f;
}

void writeToBinaryFile(FILE *fb, vectorChar *what){
	fwrite(what->vector, sizeof(char), what->count, fb);
}

/*
Stvara dinamicki byte array i vraca taj dinamicki array
@return vectorChar
*/
vectorChar *getVectorArrayFromFile(FILE *fileIn){
	char tempic[1<<12]; // koliko odjednom bajtova procitat iz filea
	int numOfReadBytes;
	vectorChar *vectorIn = (vectorChar *)malloc(sizeof(vectorChar));
	initVector(vectorIn);
	while((numOfReadBytes = fread(tempic, 1, sizeof(tempic), fileIn))){
		addElements(vectorIn, tempic, numOfReadBytes);
	}
	return vectorIn;
}

#endif