import sys
import struct
D = {}

def main():
	# procitat cijeli file u memoriju
	if len(sys.argv)!=2+1:
		print "krivo pozivanje programa. mora se stavit prvo ime jednog filea"
		print "i onda ime drugog filea u koji se zapisuje"
		return
	file1 = []
	howMuch = 1<<13
	# reading file
	with open(sys.argv[1], "rb") as f:
		while True:
			c = f.read(howMuch)
			if c:
				for b in c:
					file1.append(b)
			else:
				break
	# end reading file

	#filling starting njh
	next = 256
	for i in xrange(0, next):
		D[tuple([i])] = struct.unpack('H', struct.pack('h', i))
	start = 0
	padding = 1
	outputFile = []
	zadnji = None
	print "file je procitan"
	while start<len(file1):

		#print start
		#uzet zadnju rijec koja postoji
		pocRijec = tuple(map(lambda x: ord(x), file1[start:start+padding]))
		print len(pocRijec), pocRijec, start, padding
		if start+padding>=len(file1):
			print "sta sada"
			break
		elif pocRijec in D:
			zadnji = tuple(pocRijec) # tuple tako da napraviju kopiju
			padding+=1
		else:
			print len(file1), len(outputFile)
			# zapisivanje u file
			outputFile.append(D[zadnji])
			
			# dodavanje nove rijeci u rijecnik
			D[pocRijec] = next
			next += 1
			start += len(zadnji)
			zadnji = None
			padding = 1
	print len(file1), len(outputFile)
if __name__ == '__main__':
	main()