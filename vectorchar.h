#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"

#ifndef VECTOR_CHAR_INCLUDED
#define VECTOR_CHAR_INCLUDED

typedef struct {

	unsigned int count;
	unsigned int size;
	ushort *vector;

}vectorUShort; //wanna be c++ vector


typedef struct {

	unsigned int count;
	unsigned int size;
	char *vector;

}vectorChar; //wanna be c++ vector



void initVectorUShort(vectorUShort* vector){
	vector->size = 1;
	vector->count = 0;
	vector->vector = (ushort*)malloc(sizeof(ushort));
	if(vector->vector==NULL){
		puts("Nesto lose se desilo. Nije mogao dobit MALLOC :(\n");
		exit(-1);
	}
}

int canIAddOneUShortToVector(vectorUShort* vector){
	return vector->count+1<=vector->size;
}

void resizeUShort(vectorUShort* vector){
	vector->size *= 2;
	vector->vector = (ushort*)realloc(vector->vector, vector->size*sizeof(ushort));
	if(vector->vector==NULL){
		puts("Nesto lose se desilo. Nije mogao dobit realloc :(\n");
		exit(-1);
	}
}

void addElementsUShort(vectorUShort* vector, ushort* element, int size){
	if(size<0){
		puts("dont radit gluposti. size ne moze biti negativan USHORT\n");
		return;
	}
	while(size){
		if(canIAddOneUShortToVector(vector) == 0){ // nema mjesta za novi element
			resizeUShort(vector);
		}
		vector->vector[vector->count] = *element; // kopira element
		vector->count++;
		element++;
		size--;
	}
}

vectorUShort *convertVectorCharToUshortVector(vectorChar *arrChar, int *size){

	// sigurno ih je parno u arrCharu tako da se oko toga ne moram brinut
	(*size) = arrChar->size/2;
	vectorUShort *ret = (vectorUShort*)malloc(sizeof(vectorUShort));
	initVectorUShort(ret);
	ushort tmp;
	int i;

	for(i=0; i<(*size); i+=2){
		tmp = (arrChar->vector[i]<<8) | arrChar->vector[i+1];
		addElementsUShort(ret, &tmp, 1);
	}
	return ret;
}

// END VECTOR USHORT

void initVector(vectorChar* vector){
	vector->size = 1;
	vector->count = 0;
	vector->vector = (char*)malloc(1);
	if(vector->vector==NULL){
		puts("Nesto lose se desilo. Nije mogao dobit MALLOC :(\n");
		exit(-1);
	}
}


int canIAddOneCharToVector(vectorChar* vector){
	return vector->count+1<=vector->size;
}

void resize(vectorChar* vector){
	vector->size *= 2;
	vector->vector = (char*)realloc(vector->vector, vector->size);
	if(vector->vector==NULL){
		puts("Nesto lose se desilo. Nije mogao dobit realloc :(\n");
		exit(-1);
	}
}

void addElements(vectorChar* vector, char* element, int size){
	if(size<0){
		puts("dont radit gluposti. size ne moze biti negativan\n");
		return;
	}
	while(size){
		if(canIAddOneCharToVector(vector) == 0){ // nema mjesta za novi element
			resize(vector);
		}
		vector->vector[vector->count] = *(element);
		vector->count++;
		element++;
		size--;
	}
}

void reset(vectorChar *vector){
	vector->count = 0;
}
void vectorCopy(vectorChar *dest, vectorChar *src){
	reset(dest);
	addElements(dest, src->vector, src->count);
}

#endif