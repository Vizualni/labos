#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef HASH_INCLUDED
#define HASH_INCLUDED


#define MAXSIZE (1<<22)

typedef unsigned short ushort;


ushort nextValue = 0;

struct elementStruct{
	char *key; // tko zna koliko dugacko ce biti
	ushort val;
	unsigned int size;
	struct elementStruct *next;
};
typedef struct elementStruct element;
element hashTable[MAXSIZE];
element *dictTable[MAXSIZE];

/*
*/
unsigned int getHash(char * string, unsigned int size){
	unsigned int sol = 0;
	int counter = 1;
	while(size--){
		sol = (sol + (*string)*counter++*13337 + 5)%MAXSIZE;
		string++;
	}
	return sol;
}

int usporedi(char *p1, char *p2, int size){
	while(size--){
		if(*p1 != *p2)return 0;
		p1++; p2++;
	}
	return 1;
}

/*
Vraca broj koji se treba castat u unsigned short.
Taj broj je npr D[a]=1 <-- taj 1
@param size: koliko znakova treba procitat iz stringa
*/
int getDictionary(char *string, int size, int *found){
	unsigned int hashCode = getHash(string, size);
	//printf("hhhash %d\n", hashCode);
	element *possible = &hashTable[hashCode];
	if(possible==NULL){
		//puts("E ovo se desilo neznam kako\n");
		*found = 0;
		return -1;
	}
	do{
		if(possible->key!=NULL && possible->size == size && usporedi(string, possible->key, size)==1){
			*found = 1;
			return possible->val;
		}
		possible = possible->next;
	}while(possible);
	//printf("Nema :(");
	*found = 0;
	return -1;
}

int isInDictionary(ushort what){
	return what<nextValue;
}

// NAPISI SUTRA
void addDictionary(char *string, int size){
	unsigned int hashCode = getHash(string, size);
	//printf("next value %d\n", nextValue);
	element *possible = &hashTable[hashCode];
	if(nextValue==(ushort)(-1)){
		puts("O ou...nema vise indeksa za D");
		exit(-1);
	}
	if(possible==NULL){
		puts("Ovdje se tehicnki nebi smjelo doci");
		return;
	}
	do{
		// ovaj je zadnji
		if(possible->next==NULL){
			element *novi = (element*)malloc(sizeof(element));
			novi->key = (char*)malloc(size);
			strncpy(novi->key, string, size);
			novi->size = size;
			novi->val = nextValue++;
			novi->next = NULL;
			possible->next = novi;
			dictTable[novi->val] = novi;
			return;
		}
		possible = possible->next;
	}while(possible);
}

/*
Namjestavanje hash tablice.
Stavljanje pocetnih 256 znakova ascii tablice u tablicu
*/
void setHashTable(){
	char str[] = {'\0'};
	int i;
	for(i=0; i<MAXSIZE; i++){
		hashTable[i].key = NULL;
		hashTable[i].next = NULL;
		hashTable[i].size = 0;
	}

	for(i=0; i < 256; i++){
		str[0] = (unsigned char)i;
		addDictionary(str, 1);
	}
}

#endif