#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vectorchar.h"
#include "hash.h"
#include "files.h"




/*
Funkcija koja cita binarni file, stvara rijecnik,
zapisuje u dinamicki char array te ga vraca.
*/
vectorChar *doBrainz(vectorChar* vectorIn){
	int size;
	vectorUShort *vectorInShort = convertVectorCharToUshortVector(vectorIn, &size);
	vectorChar *bytes = (vectorChar*)malloc(sizeof(vectorChar));
	initVector(bytes);

	vectorChar *w = (vectorChar*)malloc(sizeof(vectorChar));
	initVector(w);

	vectorChar *tmp = (vectorChar*)malloc(sizeof(vectorChar));
	initVector(tmp);
	ushort code = vectorInShort->vector[0];
	
	addElements(w, dictTable[code]->key, dictTable[code]->size);
	addElements(bytes, w->vector, w->count);
	int i=1;

	while(i<size){

		code = vectorInShort->vector[i];
		if(isInDictionary(code)){
			vectorCopy(tmp, w); // tmp = w
			reset(w);
			addElements(w, dictTable[code]->key, dictTable[code]->size);
			addElements(tmp, w->vector, 1);
			addDictionary(tmp->vector, tmp->count);
		}else{
			addElements(w, w->vector, 1); // tmp = tmp + w[0]
			addDictionary(w->vector, w->count);
		}
		addElements(bytes, w->vector, w->count);
		i++;
	}

	return bytes;
}



int main(int numOfArgs, char**args){
	if(numOfArgs!=3){
		puts("Usage: imeprograma ulaznifile izlaznifile\n");
		exit(-1);
	}

	int i=0;
	for(i=0; i< MAXSIZE; i++){
		dictTable[i] = (element *)malloc(sizeof(element));
	}
	FILE *ulazna = readFile(args[1]); //args[1]);
	FILE *izlazna = openWriteBinaryFile(args[2]);
	setHashTable();
	vectorChar *vecIn = getVectorArrayFromFile(ulazna);
	vectorChar *vecOut = doBrainz(vecIn);
	writeToBinaryFile(izlazna, vecOut);

	//printf("Velicina prije %d, velicina poslije %d\n", vecIn->count, vecOut->count);

	return 0;
}
