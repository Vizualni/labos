#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int tablica[1<<8] = {}; // bolje da ih je vise, nego manje

// 256 elemenata, gdje ce svaki moci biti duljine od 10000 bitova
// makar je najveca duljina upravo log_2 od 256, a to je 8
// jer se to moze gledat kao stablo gdje svaki cvor ima dva elementa
// a najveca dubina ce onda biti upravo log_2 od 256.
typedef struct{
	int len; // sveukupna duzina
	unsigned char bytes[10000];
}bits;

bits tablicaBitova[256];

void setTablicaBitova(){
	int i, j;
	for(i=0; i<256; i++){
		tablicaBitova[i].len = 0;
		for(j=0; j<10000; j++){
			tablicaBitova[i].bytes[i]=0;
		}
	}
}

char *returnStrBits(bits *bitovi){
	char *str = (char*)malloc(10000);
	int i;
	for(i=0; i<bitovi->len; i++){
		str[i] = '0' + bitovi->bytes[i];
	}
	return str;
}

void addBit(bits *bitovi, unsigned char nulajedan){
	bitovi->bytes[bitovi->len++] = nulajedan;
}

typedef struct{
	double broj; // broj pojavljivanja
	int zeroone; // da li zapocinje sa 0 ili 1
	int i; // 0-255
} element;
unsigned int N; // sveukupni broj pojavljivanja svih elemeneata



// jednostruko povezana lista gdje su elementi sortirani

struct struct_Node{
	element *value;
	short values[256]; // index do tablice...short je tako da mogu koristit -1
	struct struct_Node *next; // sluzi za sljedeci element liste
	struct struct_Node *nule; // sluzi za strukturu stabla
	struct struct_Node *jedinice;
	double freq;
	char nulailijedan;
};
typedef struct struct_Node Node;

typedef struct{
	Node* root;
	int size;
}JednostrukaLista;


/*
Constructor
*/
JednostrukaLista* createJedLista(){
	JednostrukaLista* ret = (JednostrukaLista*)malloc(sizeof(JednostrukaLista));
	ret->root = NULL;
	ret->size = 1;
	return ret;
}

/*
Stvara element liste
*/
Node * createNode(){
	Node *ret = (Node*)malloc(sizeof(Node));
	element *el = (element*)malloc(sizeof(element));
	ret->value = el;
	ret->next = NULL;
	ret->jedinice = NULL;
	ret->nule = NULL;
	ret->freq = 0;
	int i;
	for(i=0; i<256; i++){
		ret->values[i] = -1;
	}
	return ret;
}

element *createElement(){
	element *el = (element*)malloc(sizeof(element));
	return el;
}

int cmp(Node *a, Node *b){
	if(a->freq > b->freq)return -1;
	if(a->freq < b->freq)return 1;
	return 0;
}


/*
Inserta element u listu pazeci na poredak.
*/
int insertNodeIntoList(JednostrukaLista *list, Node *node){
	Node *noviElement = node;
	Node *samoPokazivac = list->root;
	Node *onajPoslije;
	if(list->root == NULL){
		list->root =noviElement;
		list->size++;
		return 1;
	}
	// ako je ovaj sto ga mecem, veci od onog na koji node pokazuje
	if(cmp(noviElement, samoPokazivac)==1){
		noviElement->next = samoPokazivac;
		list->root = noviElement;
		list->size++;
		return 1;
	}
	do{
		// ako postoji sljedeci element
		if(samoPokazivac->next != NULL){

			// ako je sljedeci element veci od ovog trenutnog
			// onda ga mecem na to mjesto gdje se sada nalazim
			if(cmp(noviElement, samoPokazivac) == 1){
				noviElement->next = samoPokazivac->next;
				samoPokazivac->next = noviElement;
				list->size++;
				break;
			}

		}else{
		// dosao sam do kraja te je to zadnji element
			samoPokazivac->next = noviElement;
			list->size++;
			break;
		}
		samoPokazivac = samoPokazivac->next;
	}while(samoPokazivac);
	return 1;
}

int insertValueIntoList(JednostrukaLista *list, element *value){
	Node *noviElement = createNode();
	noviElement->freq = value->broj;
	noviElement->value = value;
	noviElement->values[value->i] = 1;
	return insertNodeIntoList(list, noviElement);
}

/*
Vraca prvi element
*/
Node *getFirstElement(JednostrukaLista *list){
	if(list->root == NULL){
		return NULL;
	}
	Node *ptr;

	ptr = list->root;
	list->root = ptr->next;
	list->size--;
	return ptr;
}

void printList(JednostrukaLista *list){
	Node *n = list->root;
	while(n!=NULL){
		printf("[%d %f] ->", n->value->i, n->value->broj);
		n = n->next;	
	}
	printf("%s", "\n");
}

Node *createMergedNode(Node *first, Node *second){
	Node *ret = createNode();
	int i;
	int count = 0;
	for(i=0; i<256; i++){
		if(first->values[i]>=0 || second->values[i]>=0){
			count++;
			ret->values[i] = 1;
		}
	}
	printf("pojavilo se barem %d puta\n", count);
	ret->nule = first;
	ret->jedinice = second;
	ret->freq = first->freq + second->freq;
	return ret;
}

// END JEDNOSTRUKO POVEZANA LISTA

FILE * openFile(char *filename, char* type){
	FILE *f = fopen(filename, type);
	if(!f){ exit(-1); }
	return f;
}


void printTable(){
	int i;
	for(i = 0; i<(1<<8); i++){
		printf("%d %d\n", i, tablica[i]);
	}
	printf("%s\n", "");
}

void writeTableToTxtFile(FILE *txtFile){}


void lalaRekurzija(Node *node){
	if(node==NULL){
		return;
	}
	int i=0;
	for(i=0; i<256; i++){
		if(node->values[i]>=0 && node->next!=NULL){
			addBit(&tablicaBitova[i], node->nulailijedan);
		}
	}
	lalaRekurzija(node->nule);
	lalaRekurzija(node->jedinice);
}

int main(int numOfArgs, char**args){
	FILE *ulazna = openFile("lab.pdf", "rb");
	FILE *tekstualna; // = openfile()
	int i, howMuch;

	// temp variable for reading bytes
	unsigned char ptr[1<<12];
	
	while((howMuch = fread(ptr, 1, sizeof(ptr), ulazna))){
		for(i=0; i<howMuch; i++){
			tablica[*(ptr+i)]++;
		}
	}
	// ZAPISATI ONU TABLICU U FILE
	writeTableToTxtFile(tekstualna);
	setTablicaBitova();
	//printTable();
 	
	int s = 0;
	for(i=0; i<256; i++){
			s+=tablica[i];
	}
	JednostrukaLista* lista = createJedLista();
	for(i=0; i<256; i++){
		element *el = createElement();
		el->broj = tablica[i]/((double)s);
		el->i = i;
		insertValueIntoList(lista, el);
	}

	Node *first, *second, *merged, *zadnji;
	while(1){
		first = getFirstElement(lista); // onaj koji dobije 0
		second = getFirstElement(lista); // onaj koji dobije 1
		if(first==NULL || second == NULL){
			break;
		}
		zadnji = merged = createMergedNode(first, second);
		first->nulailijedan = 0;
		second->nulailijedan = 1;
		insertNodeIntoList(lista, merged);

	}
	printf("%f\n", first->freq);
	lalaRekurzija(first);
	printf("%f\n", first->freq);
	for(i=0; i<256; i++){
		printf("%d %s\n", i, returnStrBits(&tablicaBitova[i]));
	}

	return 0;
}







